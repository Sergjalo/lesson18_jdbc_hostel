package by.epam.javatr.rdbms;

import by.epam.javatr.rdbms.bean.*;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SQLMutualInfo implements MutualInfoDao {
	ConnectionPool cp;

	public SQLMutualInfo (ConnectionPool c){
		this.cp=c;
	}	
	
	/***
	 * получим статусы по каждому месту 
	 */
	public String showFreeBeds(){
		Connection con;
		StringBuilder sb= new StringBuilder(); 
    	String sql ="call get_free_beds();";
        try {
    		con = cp.takeConnection();
        	PreparedStatement statement = con.prepareStatement(sql); 
            ResultSet rs = statement.executeQuery();
        	sb.append(String.format("%15s%17s%15s%19s%50s \n",
        	"номер места","состояние места","код заказа","состояние заказа","период использования"));		
            while (rs.next()) {
            	sb.append(String.format("%15s%17s%15s%19s%50s \n",
            			rs.getInt("num_bed"),rs.getString("bedstatus"), 
            			rs.getString("orderid")==null?" ":rs.getString("orderid"),
            			rs.getString("status")==null?" ":rs.getString("status"),
            			rs.getString("period")==null?" ":rs.getString("period")
            			));
            	
            }
            cp.closeConnection(con);
		} catch (ConnectionPoolException e1) {
			//log
        } catch (Exception e) {
        }
        return sb.toString();
	}
	
	public double get_request_sum(int id){
		return 0;
	}
	
	/***
	 * текущая цена согласно прейскуранту. 
	 * PriceParametrs - это цена аренды, брони и код цены в справочнике 
	 */
	public BedPriceParametrs getTodayPrice() {
		Connection con;
		BedPriceParametrs p;
    	String sql ="call get_today_price(?,?,?);"; //@p_rent, @p_book, @p_id
        try {
    		con = cp.takeConnection();
        	CallableStatement callSt = con.prepareCall(sql);
        	callSt.registerOutParameter("p_rent", java.sql.Types.DOUBLE);
        	callSt.registerOutParameter("p_book", java.sql.Types.DOUBLE);
        	callSt.registerOutParameter("p_id", java.sql.Types.INTEGER);
        	callSt.execute();
        	p=new BedPriceParametrs(callSt.getDouble(1),callSt.getDouble(2),callSt.getInt(3));
            cp.closeConnection(con);
        	return p;
		} catch (ConnectionPoolException e1) {
			//log
        } catch (Exception e) {
        }
		return null;
	}

}
