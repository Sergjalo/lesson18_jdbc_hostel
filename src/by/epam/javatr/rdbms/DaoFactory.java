package by.epam.javatr.rdbms;

import java.sql.Connection;


public class DaoFactory {
    private static final DaoFactory instance = new DaoFactory();
    // можно было и не делать ConnectionPool синглтоном наверное. тут же только один раз создаем. 
    ConnectionPool cp = ConnectionPool.getInstance();
    // при выполнении каждого запроса беру коннекшн из пула и в нем выполняю.
    // после получения результата - возвращаю коннекшн в пул
    
    //private ClientDao sqlClientImpl; 
    //private GoodDao sqlGoodImpl;
    private MutualInfoDao sqlMutualInfo;
    private RequestDao sqlReq;
    
    private DaoFactory() {
    	try {
			cp.initPoolData();
		} catch (ConnectionPoolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sqlMutualInfo= new SQLMutualInfo(cp);
		sqlReq = new SQLRequest(cp);
    }
    

    public static DaoFactory getInstance(){
    	return instance;
    }
    
    public MutualInfoDao getMutualInfo () {
    	return sqlMutualInfo;
    }

    public RequestDao getRequest () {
    	return sqlReq;
    }
    
    public Connection getConnection () throws ConnectionPoolException {
    	return cp.takeConnection();
    }
    
}
