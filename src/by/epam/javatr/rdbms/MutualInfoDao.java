package by.epam.javatr.rdbms;
import by.epam.javatr.rdbms.bean.*;

public interface MutualInfoDao {
	public String showFreeBeds();
	public double get_request_sum(int id);
	public BedPriceParametrs getTodayPrice() ;
}
