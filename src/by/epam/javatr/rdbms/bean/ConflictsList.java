package by.epam.javatr.rdbms.bean;

import java.util.ArrayList;
import java.util.List;


public class ConflictsList {
	private List<ConflictsInRequest> lst;

	public ConflictsList() {
		lst= new ArrayList<ConflictsInRequest>();
	}

	public ConflictsList(List<ConflictsInRequest> lst) {
		this.lst = lst;
	}

	public List<ConflictsInRequest> getLst() {
		return lst;
	}

	public void setLst(List<ConflictsInRequest> lst) {
		this.lst = lst;
	}
	
	public void add(ConflictsInRequest t) {
		lst.add(t);
	}
	
	public void clear() {
		lst.clear();
	}
	
	@Override
	public String toString() {
		StringBuilder sb= new StringBuilder(); 
       	sb.append(String.format("%15s%15s%15s%20s%20s%20s%20s%15s%15s%20s\n",
            	"ID заказа","номер места","ID конфликтный","дата начала","дата конца",
            	"дата начала конфликта","дата конца конфликта",
            	"код типа","код статуса","дата создания конфл. заказа"));
       	for (ConflictsInRequest c:lst) {
           	sb.append(c);
        }
        return sb.toString();
	}
	
}
