package by.epam.javatr.rdbms.bean;

public class BedPriceParametrs {
	// не вижу смысла заморачиваться с геттерами/сеттерами для такого простого бина
	public double p_rent; 
	public double p_book; 
	public double p_id;
	
	public BedPriceParametrs(double pr, double pb, int pi){
		p_rent=pr;
		p_book=pb;
		p_id=pi;
	}

	@Override
	public String toString() {
		return "Аренда " + p_rent + " денег за место, бронирование одного места " + p_book + " денег";
	}
	
	
	
}
