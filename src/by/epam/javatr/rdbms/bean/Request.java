package by.epam.javatr.rdbms.bean;


import java.sql.Date;
import java.util.List;

public class Request {
	public double p_rent; 
	public double p_book; 
	public double p_rentOnly; 
	public double p_toPay; 
	public int id; 
	public int clientId; 
	public Date dstart;
	public Date dend;
	public int statusId;
	public int type;
	public int idBooking;
	public List<Integer> beds;
	
	public Request(int id, double pr, double pb, double pro, double pPay){
		p_rent=pr;
		p_book=pb;
		p_rentOnly=pro;
		p_toPay=pPay;
		this.id=id;
	}

	public Request(int id){
		p_rent=0;
		p_book=0;
		p_rentOnly=0;
		p_toPay=0;
		this.id=id;
	}

	/***
	 * for request creation 
	 * @param clId
	 * @param tp
	 */
	public Request(int clId, int tp, Date st, Date en, List<Integer> b){
		clientId=clId;
		type=tp;
		beds=b;
		dstart=st;
		dend=en;
	}
	
	@Override
	public String toString() {
		return "Заказ " + id + ". К оплате " + p_toPay + " денег";
	}

}
