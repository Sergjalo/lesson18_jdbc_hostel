package by.epam.javatr.rdbms;

import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.sql.Date;
import java.util.List;

import by.epam.javatr.rdbms.bean.*;

public class ClientView {
	
	public static void main(String[] args)  {
		/*
		ConnectionPool cp = ConnectionPool.getInstance();
		try {
			cp.initPoolData();
			Connection c=cp.takeConnection();
		} catch (ConnectionPoolException e) {
			e.printStackTrace();
		}
		*/
		DaoFactory d= DaoFactory.getInstance();
		MutualInfoDao q=d.getMutualInfo();
		System.out.println(q.showFreeBeds());
		System.out.println("Сегодня цены такие:\n"+q.getTodayPrice()); 
		
		RequestDao r=d.getRequest();
		Request rs = new Request(11);
		rs= r.get_request_sum(rs); 
		System.out.println(rs);

		// создание нового заказа
		List<Integer> beds = new ArrayList <Integer>();
		beds =Arrays.asList(1,6,7,5); //это номера 100,105,200,104
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date dtS=null;
		Date dtE=null;
		try {
			
			dtS = new java.sql.Date(formatter.parse("30/12/2016").getTime());
			dtE = new java.sql.Date(formatter.parse("27/02/2017").getTime());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rs = new Request(1,1,dtS,dtE,beds);
		try {
			rs= r.createRequest(rs);
		 // если запустить два раза, то на второй раз покажет ошибку добавления заказа с конфликтными местами
		} catch (DaoException errD) {
			List<ConflictsInRequest> cnfL =errD.getConflicts();
			if (cnfL!=null) {
				System.out.println(errD.getMessage()+"\n"+
						String.format("%15s%15s%15s%20s%20s%20s%20s%15s%15s%20s\n",
            		        	"ID заказа","номер места","ID конфликтный","дата начала","дата конца",
            		        	"дата начала конфликта","дата конца конфликта",
            		        	"код типа","код статуса","дата создания конфл. заказа"));
				for (ConflictsInRequest c: cnfL) {
					System.out.print(c);
				}
			} else {
				errD.printStackTrace();
			}
		}
		
		
	}
}
