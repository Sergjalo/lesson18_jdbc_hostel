package by.epam.javatr.rdbms;

import by.epam.javatr.rdbms.bean.Request;

public interface RequestDao {
	public Request get_request_sum(Request r);
	public Request createRequest(Request r) throws DaoException;
	public String createRequestPaymentBill(int reqId);
	public String regRequestPaymentBill(int reqId);
	public void rent(int reqId);
	public void book(int reqId);
	public void accept(int reqId);
	public void doNotAccept(int reqId);
	public void banClient(int reqId);
	public void close(int reqId);
}
