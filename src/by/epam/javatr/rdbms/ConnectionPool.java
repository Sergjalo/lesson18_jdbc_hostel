package by.epam.javatr.rdbms;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public final class ConnectionPool {
		static private ConnectionPool instance = new ConnectionPool();
		private BlockingQueue<Connection> connectionQueue;
		private BlockingQueue<Connection> givenAwayConQueue;
		
		private String driverName;
		private String url;
		private String user;
		private String password;
		private int poolSize;

		private ConnectionPool() {
			poolSize = 5;
		    user = "root";//Логин пользователя
		    password = "masterkey";//Пароль пользователя
		    url = "jdbc:mysql://localhost:3306/jdbchotel?autoReconnect=true&useSSL=false";//URL адрес daotalk jdbcHotel useSSL=false
		    driverName = "com.mysql.jdbc.Driver";//Имя драйвера
		}
		
		static public ConnectionPool getInstance(){
			return instance;
		}
		
		public void initPoolData() throws ConnectionPoolException {
			try {
				Class.forName(driverName);
				givenAwayConQueue = new ArrayBlockingQueue<Connection>(poolSize);
				connectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
				for (int i = 0; i < poolSize; i++) {
					Connection connection = DriverManager.getConnection(url, user, password);
					connectionQueue.add(connection);
				}
			} catch (SQLException e) {
				throw new ConnectionPoolException("SQLException in ConnectionPool",e);
			} catch (ClassNotFoundException e) {
				throw new ConnectionPoolException("Can't find database driver class", e);
			}
		}
		
		public void dispose() {
			clearConnectionQueue();
		}
		
		private void clearConnectionQueue() {
			try {
				closeConnectionsQueue(givenAwayConQueue);
				closeConnectionsQueue(connectionQueue);
			} catch (SQLException e) {
			// logger.log(Level.ERROR, "Error closing the connection.", e);
			}
		}
		
		public Connection takeConnection() throws ConnectionPoolException {
			Connection connection = null;
			try {
				connection = connectionQueue.take();
				givenAwayConQueue.add(connection);
			} catch (InterruptedException e) {
				throw new ConnectionPoolException("Error connecting to the data source.", e);
			}
			return connection;
		}
		
		/*
		public void closeConnection(Connection con, Statement st, ResultSet rs) {
			try {
				con.close();
			} catch (SQLException e) {
			// logger.log(Level.ERROR, "Connection isn't return to the pool.");
			}
			try {
				rs.close();
			} catch (SQLException e) {
			// logger.log(Level.ERROR, "ResultSet isn't closed.");
			}
			try {
				st.close();
			} catch (SQLException e) {
			// logger.log(Level.ERROR, "Statement isn't closed.");
			}
		}
		*/
		public void closeConnection(Connection con) {
			try {
				// don't close it, just remove from givenAwayConQueue and pass to connectionQueue 
				//con.close();
				if (con.isClosed()) {
					throw new SQLException("Attempting to close closed connection.");
				}
				if (con.isReadOnly()) {
					con.setReadOnly(false);
				}
				if (!givenAwayConQueue.remove(con)) {
					throw new SQLException("Error deleting connection from the given away connections pool.");
				}
				if (!connectionQueue.offer(con)) {
					throw new SQLException("Error allocating connection in the pool.");
				}				
			} catch (SQLException e) {
			// logger.log(Level.ERROR, "Connection isn't return to the pool.");
			}
		}
		
		private void closeConnectionsQueue(BlockingQueue<Connection> queue)	throws SQLException {
			Connection connection;
			while ((connection = queue.poll()) != null) {
				if (!connection.getAutoCommit()) {
					connection.commit();
				}
				connection.close();
			}
		}
}
