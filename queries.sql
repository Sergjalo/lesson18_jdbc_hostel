/*close
но лучше различать каждый случай*/
update request r
set r.id_status=8
where r.id_status in (5,6,7)
and r.idrequest= ;

/* not accept and ban*/
update request r
set r.id_status=7
where r.id_status=4
and r.type_req=1
and r.idrequest= ;

update clients c
set c.raiting=c.raiting-1
where c.idClient= 
(
select r.id_client 
from request rget_today_priceget_request_sum
where r.idrequest= 
);

/* accept and give like*/
update request r
set r.id_status=6
where r.id_status=4
and r.type_req=1
and r.idrequest= ;

update clients c
set c.raiting=c.raiting+1
where c.idClient= 
(
select r.id_client 
from request r
where r.idrequest= 
);

/* book*/
update request r
set r.id_status=5
where r.id_status=3
and r.type_req=0
and r.idrequest= ;


/* rent*/
update request r
set r.id_status=4
where r.id_status=3
and r.type_req=1
and r.idrequest= ;

/* registerPayment*/
update request r
set r.id_status=3
where r.id_status=2
and r.idrequest= ;

/* createPayment*/
update request r
set r.id_status=2
where r.id_status=1
and r.idrequest= ;

/*
checkClient
*/
select count(*) as cnt 
from clients c
where c.idClient=1 
and c.raiting>0;

;
/*
создать запрос на аренду
preparedstatement.addBatch(       "INSERT INTO table1(all the FIELDS)"      );
preparedstatement.addBatch(       "INSERT INTO table2(all the FIELDS)"   + "VALUES(null, LAST_INSERT_ID(), ...);"  

preparedstatement = connect.prepareStatement("SELECT id FROM table3 WHERE id = LAST_INSERT_ID();"  );

      preparedstatement.executeBatch();
      resultSet = preparedstatement.executeQuery();
*/
INSERT INTO `request` (`id_client`, `id_status`, `type_req`) VALUES ('1', '1', '1');
    
SET autocommit=0;
INSERT INTO `request` (`id_client`, `dstart`, `dend`, `id_status`, `type_req`, `id_price`) VALUES ('1', '2017-01-02 00:00:00', '2017-01-03 00:00:00', '8', '1', '6');
select  LAST_INSERT_ID()
into f;
INSERT INTO `requested_beds` (`id_request`, `id_bed`) VALUES (f, '1');
INSERT INTO `requested_beds` (`id_request`, `id_bed`) VALUES (f, '2');
commit;

/* если у проверяемого заказа есть предзаказ (бронь) непроведенная до сост. Бронирован, 
то это непорядок. пусть сначала закроют бронь*/
select count(1)
from request r inner join request r2
on r.id_booking_req=r2.idrequest
where r.idrequest=13
and r2.id_status<5 ;

/* если в заказу 12 будет что-то что уже занято - вывести конфликтные места/сроки*/
/*intersection on date and bed*/
select rr.idrequest as reqCurrent, 
rr.num_bed,
rw.idrequest as reqConflicted,
rr.dstart, rr.dend,
rw.dstart as dstartConflicted, rw.dend as dendConflicted,
rw.type_req as conflType, rw.id_status as confStat, rw.date_request as confDateCreation
from
(
select r.idrequest, rb.id_bed, r.dstart, r.dend, b.num_bed, r.type_req, r.id_status, r.date_request
from request r inner join requested_beds rb 
on r.idrequest=rb.id_request
inner join beds b 
on b.idbeds=rb.id_bed
where r.idrequest<>12
)rw inner join 
(
select rb2.id_bed, r2.dstart, r2.dend, r2.idrequest, b.num_bed
from request r2 inner join requested_beds rb2 
on r2.idrequest=rb2.id_request
inner join beds b 
on b.idbeds=rb2.id_bed
where r2.idrequest=12
) rr
on rr.id_bed=rw.id_bed
where (rr.dstart between rw.dstart and  rw.dend)
or 
(rr.dend between rw.dstart and  rw.dend)
or 
(rr.dstart <=rw.dstart and  rr.dend>=rw.dend)
;

/*--   при создании перенести параметры с бронирования -*/
update request r ,
(
select r.idrequest,r2.dstart, r2.dend, r2.id_price 
from request r left join request r2
on r.id_booking_req=r2.idrequest
where r.type_req=1
and r.id_status=1
and r.id_booking_req is not null
) t
set r.dstart=t.dstart, 
	r.dend=t.dend,
    r.id_price=t.id_price
where r.idrequest= t.idrequest
and r.idrequest=13;

/*
перенести заказанные места с брони на основной заказ
*/
update requested_beds rrb ,
(
select r2.idrequest,r2.dstart, r2.dend, r2.id_price , rb.id_bed 
from request r inner join requested_beds rb 
on r.id_booking_req=rb.id_request
inner join request r2
on r.id_booking_req=r2.idrequest
where r.type_req=1
and r.id_status=1
and r.id_booking_req is not null
and r.idrequest=13
) t
set rrb.id_request=13
where rrb.id_request=t.idrequest 
;

/*---------------------------------------------------*/
;
INSERT INTO `clients` (`name`, `raiting`) VALUES ('client Name', '3');
;
/*
получить цену на сегодня
CALL get_today_price (@p_for_renting,@p_for_booking); 
select @p_for_renting,@p_for_booking;
*/
select p.price, p.booking_price
from prices p
where p.dend is null;

;
/* 
получить сумму заказа по его id
CALL get_request_sum (5, @s1,@s2,@s3); select @s1,@s2,@s3;
*/
select f.*,
if(rentSum>0,f.rentSum - f.bookingSum,0) as only_rent_sum
from (
select r.idrequest ,
DATEDIFF(r.dend,r.dstart)*sum(r.type_req*p.price) as rentSum ,
DATEDIFF(r.dend,r.dstart)*sum(!ifnull(r2.type_req,if(r.type_req,1,0))*p.booking_price) as bookingSum 
from request r inner join requested_beds rb on r.idrequest=rb.id_request
left join prices p on r.id_price=p.idprices 
left join request r2 on r.id_booking_req=r2.idrequest
group by r.idrequest
) f
;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

/*
вывести сумму заказа и сумму брони и для каждого заказа вывести номера мест.
Если была и бронь и заказ нужно учесть что бронь включена в стоимость заказа, 
Общая сумма заказа разбивается на сумму брони и сумму которую клиент оплачивал при заселении.
Учесть получать общие итоги по заказу вместе с информацией о каждом месте - нельзя, т.к. тогда 
для каждой строки будет считаться сумма - расчет сумм должен быть один раз.
*/
select header.idrequest, header.name as status,
header.rentSum as total_rent_sum, header.bookingSum, 
if(rentSum>0,header.rentSum - header.bookingSum,0) as rent_sum,
b.num_bed
from ( 
select 
r.idrequest,
s.name ,
DATEDIFF(r.dend,r.dstart)*sum(r.type_req*p.price) as rentSum ,
DATEDIFF(r.dend,r.dstart)*sum(!ifnull(r2.type_req,if(r.type_req,1,0))*p.booking_price) as bookingSum 
from request r inner join requested_beds rb on r.idrequest=rb.id_request
inner join statuses s on s.idstatuses=r.id_status
left join prices p on r.id_price=p.idprices
left join request r2 on r.id_booking_req=r2.idrequest
group by r.idrequest,
r2.idrequest,
s.name, r.type_req,r.dend,r.dstart
) header 
inner join requested_beds rb on header.idrequest=rb.id_request
inner join beds b on rb.id_bed=b.idbeds;

/*
вывести заказ аренды, бронирования, стоимость на момент заказа, сумму заказа и все номера мест в заказе
*/
select 
r.idrequest,
r.id_booking_req,
s.name,
p.price,
DATEDIFF(r.dend,r.dstart)*p.price as Sum,
b.num_bed,
r.dend, r.dstart ,
DATEDIFF(r.dend,r.dstart) as days
from request r inner join requested_beds rb on r.idrequest=rb.id_request
inner join beds b on rb.id_bed=b.idbeds
inner join statuses s on s.idstatuses=r.id_status
left join prices p on r.id_price=p.idprices
;

/*
get free beds
call get_free_beds()
*/
select b.num_bed, 
if(isnull(rentedBeds.idrequest),"Free","Rented") as bedStatus,
rentedBeds.idrequest as OrderID, 
name as Status, 
concat(dstart,"-",dend) as Period
from beds b left join 
( -- текущие 
select r.*, rb.id_bed, s.name 
from request r inner join requested_beds rb on r.idrequest=rb.id_request
inner join statuses s on s.idstatuses=r.id_status
where now() between r.dstart and r.dend
) rentedBeds on b.idbeds=rentedBeds.id_bed 
order by num_bed