package by.epam.javatr.rdbms;

import static org.junit.Assert.*;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import by.epam.javatr.rdbms.bean.ConflictsInRequest;
import by.epam.javatr.rdbms.bean.Request;


public class TestConflictOutput {
 
	@Test
	public void test() {
		// создание нового заказа
		// подготовка даннных
		List<Integer> beds = new ArrayList <Integer>();
		beds =Arrays.asList(1,6,7,5); //это номера 100,105,200,104
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		Date dtS=null;
		Date dtE=null;
		try {
			dtS = new java.sql.Date(formatter.parse("30/12/2016").getTime());
			dtE = new java.sql.Date(formatter.parse("27/02/2017").getTime());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		// получаем объект из пула соединений
		DaoFactory d= DaoFactory.getInstance();
		RequestDao r=d.getRequest();
		//создаем объект для всатвки 
		Request rs = new Request(1,1,dtS,dtE,beds);
		try {
			// выполняем SQL вставку
			rs= r.createRequest(rs);
		   // если запустить два раза, то на второй раз покажет ошибку добавления заказа с конфликтными местами
		} catch (DaoException errD) {
			List<ConflictsInRequest> cnfL =errD.getConflicts();
			if (cnfL!=null) {
				System.out.println(errD.getMessage()+"\n"+
						String.format("%15s%15s%15s%20s%20s%20s%20s%15s%15s%20s\n",
            		        	"ID заказа","номер места","ID конфликтный","дата начала","дата конца",
            		        	"дата начала конфликта","дата конца конфликта",
            		        	"код типа","код статуса","дата создания конфл. заказа"));
				for (ConflictsInRequest c: cnfL) {
					System.out.print(c);
				}
			}
			// прверка - конфликтов должно быть не меньше 11. 
			// может будет и больше если еще поназаказывают
			assertTrue(cnfL.size()>=11);
		}
		
	}

}
